# -*- coding: utf-8 -*-
"""
Created on Sat Nov 13 17:54:51 2021

@author: Stoyan Boyukliyski
"""

from deep_translator import GoogleTranslator
import pandas as pd

#Import the data
dataset = pd.read_csv('corpora.csv')

#Translate the document into english - this might be an optional
dataset['text_en'] = [GoogleTranslator(source = 'norwegian', target = 'english').translate(text[0:4999]) for text in dataset.text]

dataset.to_csv('corpora_en.csv')