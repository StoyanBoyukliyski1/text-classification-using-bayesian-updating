Logistic_Regression_sag_c1_multinomial - This gives 61% accuracy on the training set, 52% accuracy on the test set

Logistic_Regression_sag_c1000_multinomial - This gives 99.5% accuracy on the training set, 73.5% accuracy on the test set, but it runs really slowly and converges slowly

Logistic_Regression_liblinear_c1000 - This gives 99.5% accuracy on the training set, 74% accuracy on the test set

Multinomial_DB - Provides only 25% accuracy, for now it's not a great option.

Less regularization provides more accurate results on the logistic regression.