# -*- coding: utf-8 -*-
"""
Created on Tue Nov 16 14:31:23 2021

@author: Stoyan Boyukliyski
"""



import pandas as pd
import nltk
import numpy as np
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegressionCV
from sklearn.metrics import f1_score, PrecisionRecallDisplay

from sklearn.pipeline import make_pipeline
from sklearn.feature_extraction.text import TfidfVectorizer

from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt
from stop_words import get_stop_words

#Import the data
dataset = pd.read_csv('corpora.csv')

#Check the distribution of codes and see how many there are
#plt.hist(dataset.code, bins = dataset.code.shape[0])
#plt.xticks([], [])

#Look at the counts of the code 
count_codes = dataset.groupby('code').count()
sorted_list = count_codes.sort_values(by = 'text', ascending = False)

#Divide training and testing sets
train, test = train_test_split(dataset, test_size = 0.15)

Y_train = train.code.values
Y_test = test.code.values

stop_words_norway = get_stop_words('norwegian')

tfidf_vectorizer = TfidfVectorizer(max_df=0.99, norm = 'l2', stop_words=stop_words_norway)
tfidf_vectorizer.fit_transform(train.text.values)

X_train = tfidf_vectorizer.transform(train.text.values)
X_test = tfidf_vectorizer.transform(test.text.values)
print('we are here')
scikit_log_reg = LogisticRegressionCV(verbose = 1,
                                      solver='liblinear', 
                                      random_state=0, 
                                      Cs = 1000,
                                      penalty='l2', 
                                      max_iter=100)

#multi_naive_bayes = MultinomialNB() -> Achieves only 25% accuracy

model = scikit_log_reg.fit(X_train, Y_train)

labels_train = model.predict(X_train)

labels_test = model.predict(X_test)

accuracy_train = np.sum([Y_train[i] == labels_train[i] for i, key in enumerate(Y_train)])/np.sum(Y_train == Y_train)

accuracy_test = np.sum([Y_test[i] == labels_test[i] for i, key in enumerate(Y_test)])/np.sum(Y_test == Y_test)

mat = confusion_matrix(labels_test, Y_test)

print('test: ' , accuracy_train, ' train: ',  accuracy_test)